References
==========

Reading List (Books, Articles and Papers etc.)

.. code:: rust

   % cargo build
       Compiling our references v.0.0.1 (https://derefrs.connpass.com/)
       error: cannot read all references if you are alone!


&mut books
----------

| reading: The Rust Programming Language [No Starch Press, 9781593278281]
|   --> https://nostarch.com/Rust
|   　|
|   1 | `The Rust Programming Language Second Edition`_ (rust-lang.org)
|   2 | `The Rust Programming Language 2018 Edition`_ (rust-lang.org)
|   = note: currently we're reading \`second edition\` by now ;)
|
| reading: Mastering Rust [Packt Publishing, 9781785885303]
|   --> https://www.packtpub.com/application-development/mastering-rust
|   　|
|   1 | `PacktPublishing/Mastering-Rust - GitHub`_
|
| reading: Hands-On Concurrency with Rust [Packt Publishing, 9781788399975]
|   --> https://www.packtpub.com/application-development/hands-concurrency-rust
|   　|
|   1 | `PacktPublishing/Hands-On-Concurrency-with-Rust - GitHub`_
|
| reading: Hands-On Functional Programming in Rust [Pakct Publishing, 9781788839358]
|   --> https://www.packtpub.com/application-development/hands-functional-programming-rust
|   　|
|   1 | `PacktPublishing/Hands-On-Functional-Programming-in-Rust - GitHub`_

.. _`The Rust Programming Language Second Edition`: https://doc.rust-lang.org/book/#second-edition
.. _`The Rust Programming Language 2018 Edition`: https://doc.rust-lang.org/book/2018-edition/index.html
.. _`PacktPublishing/Mastering-Rust - GitHub`: https://github.com/PacktPublishing/Mastering-Rust
.. _`PacktPublishing/Hands-On-Concurrency-with-Rust - GitHub`: https://github.com/PacktPublishing/Hands-On-Concurrency-with-Rust
.. _`PacktPublishing/Hands-On-Functional-Programming-in-Rust - GitHub`: https://github.com/PacktPublishing/Hands-On-Functional-Programming-in-Rust

&mut feeds
----------

| reading: Read Rust
|   --> https://readrust.net/
|
| reading: This Week in Rust
|   --> https://this-week-in-rust.org/
|
| reading Awesome Rust
|   --> https://rust.libhunt.com/
|
| reading: Fearless Rust Bloggers
|   --> https://github.com/rayascott/fearless-rust-bloggers

&mut others
-----------

| reading: Rust Documentation
|   --> https://www.rust-lang.org/en-US/documentation.html
